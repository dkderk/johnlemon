﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour
{
    private Canvas CanvasObject; 

    void Start()
    {
        CanvasObject = GetComponent<Canvas>();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            CanvasObject.enabled = !CanvasObject.enabled;
        }
    }
}